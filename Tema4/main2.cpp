#include <iostream>
#include "lista_simpla.cpp"
using namespace std;
 
int main()
{
    ListaD myList; // Creez o lista
    myList.push_back(8);
    myList.push_front(5);
    myList.push_back(11);
 
    for(ListaD::Iterator it = myList.front(); it != ListaD::END; it++)
        cout << *it << ' ';
    cout << '\n';
 
    ListaD::Iterator p = myList.search(8);
    myList.insert_after(16, p);
    myList.insert_before(22, p);
 
    // Afisez invers
    for(ListaD::Iterator it = myList.back(); it != ListaD::END; it--)
        cout << *it << ' ';
    cout << '\n';
 
    p = myList.search(8);
    myList.remove(p);
    myList.pop_back();
    myList.pop_front();
 
    // Afisez invers
    for(ListaD::Iterator it = myList.back(); it != ListaD::END; it--)
        cout << *it << ' ';
    cout << '\n';
 
    // Afisez normal
    for(ListaD::Iterator it = myList.front(); it != ListaD::END; it++)
        cout << *it << ' ';
    cout << '\n';
 
    
    return 0;
}
