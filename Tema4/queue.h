#include <stdio.h>

template <typename T, int N>
class Queue { 
private: 
	int head;
	int tail;
	int size;
	T queueArray[N];

public: 
	//Le Constructor
	Queue();

	//Le Destructor
	~Queue();

	void enqueue (T e );
	T dequeue();
	T front();
	int isEmpty();
	int isFull();
	void show();

};
