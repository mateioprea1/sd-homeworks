#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_

template <typename T>
struct element
{
	T info;
	struct element *next;
	struct element *prev;
};
template <typename T>
class LinkedList
{
	element <T> *first, *last;
public:
	LinkedList();
	~LinkedList();
	void addFirst(T );
	void addLast(T );
	void removeFirst();
	void removeLast();
	element<T>* returneaza();
	element<T>* findFirstOccurence(T );
	element<T>* findLastOccurence(T );
	void removeFirstOccurence(T );
	void removeLastOccurence(T );
	int isEmpty();
	T *getFirst();
	T *getLast();
};

#endif
