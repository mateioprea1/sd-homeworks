template<typename T>
class Coada
{
public:
    Coada() { // Constructor
        cap = baza = NULL; // coada vida
        count = 0; // niciun element
    }
    ~Coada(); // Destructor
    void enqueue(const T& ob); // Adaug un element in coada
    void dequeue(); // Elimin obiectul din fata
    const T& front() const { // Returnez o referinta catre cap
        if(isEmpty()) throw "Eroare! Coada Vida!";
        return cap->data;
    }
    const T& back() const { // Returnez o referinta catre baza
        if(isEmpty()) throw "Eroare! Coada Vida!";
        return baza->data;
    }
    int size() const { return count; } // Returnez numarul de elemente
    bool isEmpty() const { return cap == NULL; } // Este coada vida?
private:
    struct Element
    {
        T data;
        Element * next;
    };
    Element * cap;
    Element * baza;
    int count; // Numar elementele
};
 
template<typename T> Coada<T>::~Coada()
{
    while(cap != NULL)
    {
        Element * q = cap;
        cap = cap->next;
        delete q;
    }
}
 
template<typename T> void Coada<T>::enqueue(const T& ob)
{
    if(isEmpty()) // Daca coada este vida
    {
        cap = new Element;
        cap->data = ob;
        cap->next = NULL; // Fiind singurul element, succesorul este NULL
        baza = cap;
        count = 1;
    }
    else
    {
        Element * p = new Element;
        p->data = ob;
        p->next = NULL; // Devine noul element baza
        baza->next = p; // Fosta baza se leaga de noua baza
        baza = p; // p devine baza
        ++count;  // S-a mai adaugat un element
    }
}
 
template<typename T> void Coada<T>::dequeue()
{
    if(isEmpty()) throw "Eroare! Coada Vida!";
    Element * q = cap; // Salvez elementul din cap
    cap = cap->next; // Elementul urmator devine cap
    delete q;
    --count;
}
