#include "linkedList.h"
#include <stdio.h>
#include <stdlib.h>
template <typename T>
LinkedList<T>::LinkedList()
{
	first=last=NULL;
}
template <typename T>
LinkedList<T>::~LinkedList()
{/*
	element <T> *p=first;
	if(first==NULL)
		return;
	while(p->next!=NULL)
	{
		p=p->next;
		delete p->prev;
	}
	delete last;
*/}
template <typename T>
element<T>* LinkedList<T>::returneaza()
{
return first;
}
template <typename T>
void LinkedList<T>::addFirst(T x)
{
	element<T> *p;
	if(first==NULL)
	{
		first=new element<T>;
		last=first;
		first->info=x;
		first->prev=first;
		first->next=first;
	}
	else
	{
		p=new element<T>;
		p->info=x;
		p->prev=last;
		first->prev=p;
		p->next=first;
		first=p;
	}
}

template<typename T>
void LinkedList<T>::addLast(T x)
{
	element<T> *p;
	if(last==NULL)
	{
		first=last=new element<T>;
		last->info=x;
		last->prev=last;
		last->next=last;
	}
	else
	{
		p=new element<T>;
		p->next=first;
		p->prev=last;
		p->info=x;
		last->next=p;
		last=p;
	}
}

template <typename T>
void LinkedList<T>::removeFirst()
{
	element<T> *p;
	if(first==NULL)
	{
		//fprintf(stderr,"EROARE: Lista este deja goala");
		return;
	}
	else
	{
		if(first==last)
		{
			delete first;
			first=last=NULL;
		}
		else
		{
			(first->next)->prev=last;
			p=first->next;
			delete first;
			first=p;
			last->next=first;
		}
	}
}

template<typename T>
void LinkedList<T>::removeLast()
{
	element<T> *p;
	if(last==NULL)
	{
		//fprintf(stderr,"EROARE: Lista este deja goala");
		return;
	}
	else
	{
		if(first==last)
		{
			delete last;
			first=last=NULL;
		}
		else
		{
			(last->prev)->next=first;
			p=last->prev;
			delete last;
			last=p;
			first->prev=last;
		}
	}
}

template<typename T>
element<T> *LinkedList<T>::findFirstOccurence(T x)
{
	element<T> *p=first;
	if (first==NULL)
	{
		//fprintf(stderr,"EROARE:Lista e goala!");
		return NULL;
	}
	else
	{
		while (p!=first && p->info!=x)
		{
			p=p->next;
		}
		if(p==first)
		{
		//	fprintf(stderr,"1.EROARE! Lista nu il contine pe x");
			return NULL;
		}
		return p;
	}
}

template <typename T>
element<T>* LinkedList<T>::findLastOccurence(T x)
{
	element<T> *p=last;
	if (last==NULL)
	{
		//fprintf(stderr,"EROARE:Lista e goala!");
		return NULL;
	}
	else
	{
		while (p!=last && p->info!=x)
		{
			p=p->prev;
		}
		if(!p)
		{
		//	fprintf(stderr,"2.EROARE! Lista nu il contine pe x");
			return NULL;
		}
		return p;
	}
}
template <typename T>
void LinkedList<T>::removeFirstOccurence(T x)
{
	element<T> *p=findFirstOccurence(x);
	if(p==NULL)
	{
		return;
	}
	else
	{
		if(p->prev==last)
		{
			removeFirst();
		}
		else if(p->next==first)
		{
			removeLast();
		}
		else
		{
			p->prev->next=p->next;
			p->next->prev=p->prev;
			delete p;
		}
	}
}

template <typename T>
void LinkedList<T>::removeLastOccurence(T x)
{
	element<T> *p=findLastOccurence(x);
	if(p==NULL)
	{
		return;
	}
	else
	{
		if(p->prev==last)
		{
			removeFirst();
		}
		else if(p->next==first)
		{
			removeLast();
		}
		else
		{
			p->prev->next=p->next;
			p->next->prev=p->prev;
			delete p;
		}
	}
}
template <typename T>
int LinkedList<T>::isEmpty()
{
	return (first==NULL);
}

template <typename T>
T *LinkedList<T>::getFirst()
{
	if(!isEmpty())
	{
		return &(first->info);
	}
	return NULL;
}

template <typename T>
T *LinkedList<T>::getLast()
{
	if(!isEmpty())
	{
		return &(last->info);
	}
	return NULL;
}

template class LinkedList<int>;
